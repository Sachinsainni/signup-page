import React from "react";

export default function Select(props) {
  return (
    <>
      <label htmlFor={props.id}>{props.label} </label>
      <select
        name={props.id}
        id={props.id}
        className="select-input all"
        onChange={props.onChangeHandler}
      >
        {
            props.option
            .map((data,idx) => {
                return <option key={idx} value={data}>{data}</option>
            })
        }
      </select>
    </>
  );
}
