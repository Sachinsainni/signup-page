import React, { Component } from "react";
import validator from "validator";
import Botton from "./Botton";
import NameInput from "./NameInput";
import Select from "./Select";

export default class Form extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: "",
      firstNameError: "",
      lastName: "",
      lastNameError: "",
      age: "",
      ageError: "",
      email: "",
      emailError: "",
      password: "",
      passwordError: "",
      repeatPassword: "",
      repeatPasswordError: "",
      agreement: false,
      agreementError: "",

      success: {
        display: "none",
      },

      form: {
        display: "block",
      },
    };
  }

  onChangeHandler = (event) => {
    this.setState({
      [event.target.id]: event.target.value,
    });
  };

  onSubmitform = () => {
    console.log(this.state.firstName);
    let valid = 0;

    if (!validator.isAlpha(this.state.firstName)) {
      this.setState({
        firstNameError: "Please enter a first name ",
      });
      valid += 1;
    }

    if (!validator.isAlpha(this.state.lastName)) {
      this.setState({
        lastNameError: "Please enter a last name",
      });
      valid += 1;
    }

    if (!validator.isInt(this.state.age)) {
      this.setState({
        ageError: "Please enter valid age",
      });
      valid += 1;
    }

    if (!validator.isEmail(this.state.email)) {
      this.setState({
        emailError: "Please enter email",
      });
      valid += 1;
    }

    if (!validator.isStrongPassword(this.state.password)) {
      this.setState({
        passwordError: "Please enter strong password",
      });
      valid += 1;
    }

    if (!this.state.password === this.state.repeatPassword) {
      this.setState({
        repeatPasswordError: "Password not match",
      });
      valid += 1;
    }

    if (this.state.agreement !== true) {
      this.setState({
        agreementError: "Check for Agreement",
      });
    }

    if (valid === 0) {
      this.setState({
        success: {
          display: "block",
          color: "green",
        },
        form: {
          display: "none",
        },
      });
    }
  };

  render() {
    return (
      <>
        <div id="content-container" style={this.state.form}>
          <div id="form-container">
            <NameInput
              id={"firstName"}
              label={"First Name"}
              placeholder={"first name"}
              onChangeHandler={this.onChangeHandler}
              inputError={this.state.firstNameError}
              icon="fa-regular fa-user"
            />

            <NameInput
              id={"lastName"}
              label={"Last Name"}
              placeholder={"last name"}
              onChangeHandler={this.onChangeHandler}
              inputError={this.state.lastNameError}
              icon="fa-regular fa-user"
            />

            <NameInput
              id={"age"}
              label={"Age"}
              placeholder={"age"}
              onChangeHandler={this.onChangeHandler}
              inputError={this.state.ageError}
              icon = "fa-solid fa-list-ol"
            />
            <Select
              id="gender"
              label = "Gender"
              onChangeHandler={this.onChangeHandler}
              option={["Male", "Female", "Other"]}
            />

            <Select
              id="role"
              label ="Role"
              onChangeHandler={this.onChangeHandler}
              option={[
                "Developer",
                "Senior Developer",
                "Lead Engineer",
                "CTO",
                "Other",
              ]}
            />

            <NameInput
              id="email"
              label="Email"
              placeholder="email"
              onChangeHandler={this.onChangeHandler}
              inputError={this.state.emailError}
              icon = "fa-regular fa-envelope"
            />

            <NameInput
              id="password"
              label="Password"
              type = "password"
              placeholder="password"
              onChangeHandler={this.onChangeHandler}
              inputError={this.state.passwordError}
              icon ="fa-solid fa-key"
            />

            <NameInput
              id="repeatPassword"
              type = "password"
              label="Repeat Password"
              placeholder="repeat password"
              onChangeHandler={this.onChangeHandler}
              inputError={this.state.repeatPasswordError}
              icon ="fa-solid fa-key"
            />

            <label htmlFor="agreement">Agree to Terms and Conditions </label>
            <input
              type="checkbox"
              name="agreement"
              id="agreement"
              className="check-input"
              onChange={this.onChangeHandler}
            />
            <span>
              {this.state.agreementError.length > 0 ? (
                <div className="error">{this.state.agreementError}</div>
              ) : (
                <div> &nbsp;</div>
              )}
            </span>

            <Botton click={this.onSubmitform} />
          </div>
        </div>
        <h1 style={this.state.success}>Congratulation! You're great!</h1>
      </>
    );
  }
}
