import React from "react";

export default function NameInput(props) {
  return (
    <>
      <label htmlFor={props.id}>{props.label}</label>
      <div className="input-holder">
        <input
          type={props.type}
          placeholder={`Enter Your ${props.placeholder} `}
          id={props.id}
          className="all name-input"
          onChange={props.onChangeHandler}
        />
        <i className={props.icon}></i>
      </div>
      <span>
        {props.inputError.length > 0 ? (
          <div className="error">{props.inputError}</div>
        ) : (
          <div>&nbsp;</div>
        )}
      </span>
    </>
  );
}
