import Form from './component/Form';
import Header from './component/Header'
import './App.css';

function App() {
  return (
    <>
    <Header/>
    <Form/>
    </>
  );
}

export default App;
